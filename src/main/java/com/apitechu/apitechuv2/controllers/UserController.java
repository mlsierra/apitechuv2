package com.apitechu.apitechuv2.controllers;


import com.apitechu.apitechuv2.Services.UserService;
import com.apitechu.apitechuv2.models.ProductModel;
import com.apitechu.apitechuv2.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*" , methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,RequestMethod.DELETE})
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderby", required = false) String orderBy
    ){
        System.out.println("getUsers");
        System.out.println("El valor de $orderby es " + orderBy);
        return new ResponseEntity<>(this.userService.getUsers(orderBy), HttpStatus.OK) ;
    }



    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar " + id);

        Optional<UserModel> result = this. userService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>( "Usuario no encontrado", HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuario que se va crear a es " + user.getId());
        System.out.println("El nombre del usuario que se va crear a es " + user.getName());
        System.out.println("La edd del usuario que se va a crear es " + user.getAge());
        return new ResponseEntity<>( this.userService.add(user), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id en parametro  de la url es " + id);
        System.out.println("La id del usuario a que se va a actualizar es " + user.getId());
        System.out.println("El nombre  del usuario que se va a actualizar es " + user.getName());
        System.out.println("La edad  del usuario que se va a actualizar es " + user.getAge());

        Optional<UserModel> result = this.userService.findById(id);

           if (result.isPresent() == true) {
                return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Usuario no encontrado para actualizar", HttpStatus.NOT_FOUND);
            }

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("delteteUeer");
        System.out.println("La id del usuario a borrar " + id);

        // Optional<ProductModel> result = this. productService.findById(id);
        boolean deleteUser = this.userService.delete(id);

        if (deleteUser == true){
            return new ResponseEntity<>( "Usuario borrado", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Usuario no encontrado para borrar", HttpStatus.NOT_FOUND);
        }

    }

}
