package com.apitechu.apitechuv2.controllers;

import com.apitechu.apitechuv2.Apitechuv2Application;
import com.apitechu.apitechuv2.Services.ProductService;
import com.apitechu.apitechuv2.models.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*" , methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,RequestMethod.DELETE})
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProduct(){
        System.out.println("getProducts");

        return this.productService.findAll();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar " + id);

        Optional<ProductModel> result = this. productService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>( "Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id del producto que se va crear a es " + product.getId());
        System.out.println("La descripcion  del producto que se va crear a es " + product.getDesc());
        System.out.println("El precio  del producto que se va a crear es " + product.getPrice());
        return new ResponseEntity<>( this.productService.add(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("La id en parametro  de la url es " + id);
        System.out.println("La id del producto a que se va a actualizar es " + product.getId());
        System.out.println("La descripcion  del producto que se va a actualizar es " + product.getDesc());
        System.out.println("El precio  del producto que se va a actualizar es " + product.getPrice());

          Optional<ProductModel> result = this.productService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>( this.productService.update(product), HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Producto no encontrado para actualizar", HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("delteteProduct");
        System.out.println("La id del producto a borrar " + id);

       // Optional<ProductModel> result = this. productService.findById(id);
        boolean deleteProduct = this.productService.delete(id);

        if (deleteProduct == true){
             return new ResponseEntity<>( "Producto borrado", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Producto no encontrado para borrar", HttpStatus.NOT_FOUND);
        }

    }






}
