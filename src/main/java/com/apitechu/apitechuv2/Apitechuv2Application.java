package com.apitechu.apitechuv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apitechuv2Application {

	public static void main(String[] args) {
		SpringApplication.run(Apitechuv2Application.class, args);
	}

}
