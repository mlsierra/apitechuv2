package com.apitechu.apitechuv2.Services;

import com.apitechu.apitechuv2.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class PurchaseServiceResponse {
    private String msg;
    private PurchaseModel purchase;
    private HttpStatus httpStatus;
    public PurchaseServiceResponse() {
    }
    public String getMsg() {
        return this.msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public PurchaseModel getPurchase() {
        return this.purchase;
    }
    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }
    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
