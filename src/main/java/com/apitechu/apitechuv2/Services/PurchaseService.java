package com.apitechu.apitechuv2.Services;

import com.apitechu.apitechuv2.models.PurchaseModel;
import com.apitechu.apitechuv2.repositories.ProductRepository;
import com.apitechu.apitechuv2.repositories.PurchaseRepository;
import com.apitechu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserRepository userRepository;
    @Autowired
    ProductRepository productRepository;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
            System.out.println("addPurchase");
            PurchaseServiceResponse result = new PurchaseServiceResponse();
            result.setPurchase(purchase);
            if (this.userRepository.findById(purchase.getUserId()).isEmpty() == true) {
                System.out.println("El usuario de la compra no se ha encontrado");
                result.setMsg("El usuario de la compra no se ha encontrado");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                return result;
            }
            if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
                System.out.println("Ya hay una compra con esa id");
                result.setMsg("Ya hay una compra con esa id");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                return result;
            }
            float amount = 0;
            for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
                if (this.productRepository.findById(purchaseItem.getKey()).isEmpty()) {
                    System.out.println("El producto con la id " + purchaseItem.getKey() + " no existe");
                    result.setMsg("El producto con la id " + purchaseItem.getKey() + " no existe");
                    result.setHttpStatus(HttpStatus.BAD_REQUEST);
                    return result;
                } else {
                    System.out.println("Añadiendo valor de " + purchaseItem.getValue() +
                            " unidades del producto al total");
                    amount += this.productRepository.findById(purchaseItem.getKey()).get().getPrice()
                            * purchaseItem.getValue();
                }
            }
            purchase.setAmount(amount);
            this.purchaseRepository.save(purchase);
            result.setMsg("Compra añadida correctamente");
            result.setHttpStatus(HttpStatus.CREATED);
            return result;
        }
}
