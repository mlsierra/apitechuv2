package com.apitechu.apitechuv2.Services;

import com.apitechu.apitechuv2.models.UserModel;
import com.apitechu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserModel> getUsers(String orderBy) {
        System.out.println("getUsers");
        List<UserModel> result;

        if (orderBy != null ){
            System.out.println("Se ha pedido ordenacion");
            result = this.userRepository.findAll(Sort.by("age"));

        } else {
            result = this.userRepository.findAll();
        }
        return result;
    }


    public Optional<UserModel> findById(String id){
        System.out.println("findByID");
        System.out.println("Obteniendo el usuario con la id " + id);
        return this.userRepository.findById(id);
    }

    public  UserModel add(UserModel user){
        System.out.println("add");
        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user){
        System.out.println("update");
        System.out.println("Actualizando el usuario con la id " + user.getId());
        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete");
        boolean result = false ;
        if(this.userRepository.findById(id).isPresent() == true){
            System.out.println("Borrando el usuario con la id " + id);
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }

}
