package com.apitechu.apitechuv2.repositories;

import com.apitechu.apitechuv2.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String > {

}
