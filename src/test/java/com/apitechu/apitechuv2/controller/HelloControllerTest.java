package com.apitechu.apitechuv2.controller;

import com.apitechu.apitechuv2.controllers.HelloController;
import org.junit.Assert;
import org.junit.Test;


public class HelloControllerTest {
    @Test
    public void testHellor(){

        HelloController sut = new HelloController();

        Assert.assertEquals("Hola Laura!", sut.hello("Laura"));
    }
}
